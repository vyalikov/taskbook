<?php

require_once("application/views/view_task.php");
require_once("application/models/model_task.php");

class Controller_Task extends Controller
{
	function __construct()
	{
		$this->view = new View_Task();
		$this->model = new Model_Task(array('order' => 'name'));
	}

	public function action_index()
	{
		$id = 0;
		$limit1 = 0;
		$page = 1;
		if (isset($_GET['p']))
			$page = (int)$_GET['p'];
		$limit1 = ($page - 1) * 3;
		$order_by = 'id';
		if (isset($_GET['sort']))
			$order_by = $_GET['sort'];
		$order_direction = 'desc';
		if (isset($_GET['sort_direction']))
			$order_direction = $_GET['sort_direction'];

		$data = array();

		$this->model->query(array("order" => $order_by, "order_direction" => $order_direction, "limit" => $limit1.",3"));
		$data['result'] = $this->model->getAllRows();
		$data['count'] = $this->model->getCount();
		$data['page'] = $page;
		$data['sort'] = $order_by;
		$data['sort_direction'] = $order_direction;

		$this->view->generate_index($data);
	}
		
	public function action_create()
	{
		$data = null;
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
			$data = $this->model->create_task();
		$this->view->generate_create($data);
	}
		
	public function action_edit()
	{
		$data = null;
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
			$data = $this->model->edit_data();
		else
		{
			$data = $this->model->get_data_for_edit($_GET['id']);
		}

		$data['id'] = (int)htmlspecialchars($_GET['id']);
		$this->view->generate_edit($data);
	}
}

?>