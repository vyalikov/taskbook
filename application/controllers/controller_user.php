<?php

include_once 'application/models/model_user.php';
include_once 'application/views/view_user.php';

class Controller_user extends Controller
	{
		public function __construct()
		{
			$this->model = new Model_User();
			$this->view = new View_User();
		}
		public function action_login()
		{
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
				$data = $this->model->check_login_password();
			$data['is_admin'] = $this->model->is_admin();
			$this->view->generate_login($data);
		}
		public function action_logout()
		{
			$this->model->logout();
			$this->view->generate_logout();
		}
	}

?>