<?php

class Model_Task extends Model {
     
    const STATUS_IN_PROGRESS = 0;
    const STATUS_COMPLETE = 1;

    const EDITED_NOT_CHANGED = 0;
    const EDITED_WAS_EDITED = 1;

    /** 
    * Task id
    * @var int
    */
    public $id;

    /** 
    * User name
    * @var string
    */
    public $name;

    /** 
    * User email
    * @var string
    */
    public $email;

    /** 
    * Task description
    * @var string
    */
    public $description;

    /** 
    * Task status
    * @var int {Model_Task::STATUS_}
    */
    public $status = Model_Task::STATUS_IN_PROGRESS;

    /** 
    * Task status
    * @var int {Model_Task::EDITED_}
    */
    public $edited;


    public function fieldsTable(){
        return array(
            'id' => 'Id',
            'name' => 'First name',
            'email' => 'Email',
            'description' => 'Task description',
            'status' => 'Task status',
            'edited' => 'Task edited by admin'
        );
    }

    /** 
    * Validating data and create new task record at database if data is correct
    *
    * @return array (<int> success {0, 1} , <string> error_text)
    */
    public function create_task()
    {
        $res = $this->validate();
        if ($res['success'] == 0)
            return ($res);

        $res = $this->create_new();
        return $res;
    }

    /** 
    * Get data to display at edit task page
    *
    * @return array (<array> result, <int> id, <int> success {0, 1} , <string> error_text)
    */
    public function get_data_for_edit($id)
    {
        $id = htmlspecialchars($id);

        $data = array();

        require_once("application/models/model_user.php");
        $model_user = new Model_User();
        $is_admin = $model_user->is_admin();

        if ($is_admin == 0)
            return (array('success'=>0, 'error_text'=>'Для редактирования задачи необходимо быть администатором! Войдите на странице авторизации.'));

        $data['result'] = $this->getRowById($id);
        $data['id'] = $id;

        return ($data);
    }

    /** 
    * Updates task record in database or returns an error text if have errors
    *
    * @return array ( <int> success {0, 1} , <string> error_text)
    */
    public function edit_data()
    {
        require_once("application/models/model_user.php");
        $model_user = new Model_User();
        $is_admin = $model_user->is_admin();

        if ($is_admin == 0)
            return (array('success'=>0, 'error_text'=>'Для редактирования задачи необходимо быть администатором! Войдите на странице авторизации.'));
            
        $id = (int)htmlspecialchars($_GET['id']);


        $new_description = strip_tags($_POST['description']);
        $new_description = trim($new_description);
        $new_description = htmlspecialchars($new_description); 

        $task_row = $this->getRowById($id);
        $old_description = $task_row['description'];

        $status = Model_Task::STATUS_IN_PROGRESS;

        if (isset($_POST['status']) && (int)$_POST['status'] == 'on')
             $status = Model_Task::STATUS_COMPLETE;


        $this->id = $id;
        $this->description = $new_description;
        if ($old_description == $new_description)
        {
             $this->edited = Model_Task::EDITED_NOT_CHANGED;
        }
        else 
        {
            $this->edited = Model_Task::EDITED_WAS_EDITED;
        }

        $this->status = $status;

        $query_res = $this->update();

        $ret = array('success'=>1, 'error_text'=>'');

        if(!$query_res)
        {
           $ret = array('success'=>0, 'error_text'=>'Ошибка изменения задачи');
        }

        return $ret;
    }

    /** 
    * Adds new task record in database or returns an error text if have errors
    *
    * @return array ( <int> success {0, 1} , <string> error_text)
    */
    private function create_new()
    {
        $description = htmlspecialchars($_POST['description']);
            
        $this->name = $_POST['username'];
        $this->email = $_POST['email'];
        $this->description = $description;   
        $this->status = Model_Task::STATUS_IN_PROGRESS;

        $res = $this->save();

        if (!$res)
            return (array('success'=>0, 'error_text'=>'Не удалось добавить запись в базу данных'));

         return (array('success'=>1, 'error_text'=>''));
     }

    /** 
    * Checks user input data and return an error text if have errors
    *
    * @return array ( <int> success {0, 1} , <string> error_text)
    */
    public function validate()
        {
            $_POST['description'] = strip_tags($_POST['description']);
            $_POST['description'] = trim($_POST['description']);
            if (!preg_match("/^[a-z]{1,}+[a-z]{0,24}$/iu", $_POST['username']))
                return (array('success'=>0, 'error_text'=>'Ник должен быть не длиннее 24 символов, используйте латинские буквы.'));
            else if (!preg_match("/.+@.+\..+/iu", $_POST['email']))
                return (array('success'=>0, 'error_text'=>'Введите корректный E-mail.'));
            else if (mb_strlen($_POST['description']) < 1)
                return (array('success'=>0, 'error_text'=>'Введите описание задачи.'));

            return (array('success'=>1, 'error_text'=>''));
        }
     
}

?>