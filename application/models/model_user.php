<?php

	class Model_User extends Model
	{
		public function is_admin()
		{
			if (isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == 1)
				return (1);
			else
				return (0);
		}

		public function check_login_password()
		{
			
			if (!empty($_POST['login']))
				$login = $_POST['login'];
			if (!empty($_POST['password']))
				$password = $_POST['password'];

			if(!isset($login))
			{
				return(array('success'=>0, 'error_text'=>"Введите логин."));
			}

			if(!isset($password))
			{
				return(array('success'=>0, 'error_text'=>"Введите пароль."));
			}

			if ($login == 'admin' && $password == '123')
			{
				$_SESSION['is_admin'] = 1;
				return(array('success'=>1, 'error_text'=>''));
			}
			else
				return(array('success'=>0, 'error_text'=>"Неверный логин или пароль."));
		}

		public function logout()
		{
			$_SESSION['is_admin'] = 0;
		}
	}
?>