<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title><?php echo $title ?></title>

    <!-- Application style -->
    <link rel="stylesheet" href="/css/style.css">

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
	<!-- Latest compiled and minified JavaScript -->
	<script src="/js/jquery-3.4.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

	<!-- Custom stylesheet -->
	<link href="/css/style.css" rel="stylesheet">
  </head>
  <body>
  <nav class='navbar navbar-default'>
	<div class='container-fluid'>
		<div class='navbar-header'>
			<a class='navbar-brand' href='/'>
				Задачник
			</a>
		</div>
		<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
			<ul class="nav navbar-nav">
				<li><a href='/task/create'>Создать задачу</a></li>
				<?php
					include_once 'application/models/model_user.php';
					$model_user = new Model_user();
					if ($model_user->is_admin() == 0)
						echo "
							<li><a href='/user/login'>Войти как администратор</a></li>
							";
					else
						echo "
							<li><a href='/user/logout'>Выйти из аккаунта администратора</a></li>
							";
				?>
			</ul>
		</div>
	</div>
  </nav>