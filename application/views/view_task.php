<?php

class View_Task extends View
	{
		function sort_params_div($page)
		{
			return "
				<div class='sorting-params'>
						<span class='sorting-title'>Сортировать по: </span>
						<span class='sorting-param'>
							<span class='name'>Имя пользователя</span>
							<a href='/task/index?sort=name&sort_direction=asc&p=$page' class='btn btn-default'>A-Z</a>
							<a href='/task/index?sort=name&sort_direction=desc&p=$page' class='btn btn-default'>Z-A</a>
						</span>
						<span class='sorting-param'>
							<span class='name'>Email</span>
							<a href='/task/index?sort=email&sort_direction=asc&p=$page' class='btn btn-default'>A-Z</a>
							<a href='/task/index?sort=email&sort_direction=desc&p=$page' class='btn btn-default'>Z-A</a>
						</span>
						<span class='sorting-param'>
							<span class='name'>Статус</span>
							<a href='/task/index?sort=status&sort_direction=asc&p=$page' class='btn btn-default'>Вверху невыполненные</a>
							<a href='/task/index?sort=status&sort_direction=desc&p=$page' class='btn btn-default'>Вверху выполненные</a>
						</span>
				</div>
			";
		}
		
		function pagination_div($count, $current, $sort = null, $sort_direction = null)
		{
			$pages = '';

			$sort_params = '';
			if (isset($sort) && $sort){
				$sort_params.= "&sort=$sort";
			}
			if (isset($sort_direction) && $sort_direction){
				$sort_params.= "&sort_direction=$sort_direction";
			}
			
			$count_pages = (int)($count - 1) / 3 + 1;
			$i = 1;
			while ($i <= $count_pages)
			{
				$class='btn-default';
				if ($i == $current)
					$class = 'btn-info';
				$pages.="<a href='/task/index?p=$i$sort_params' class='btn $class'>$i</a>";
				$i++;
			}
			$res = "
				<div class='pagination'>
					Страницы: $pages
				</div>
			";
			return ($res);
		}
		function generate_index($data = null)
		{
			$title = 'Список задач';

			include_once 'application/views/template/header.php';

			echo '<h1>Список задач</h1>';
			
			include_once 'application/models/model_user.php';
			$model_user = new Model_user();
			$is_admin = $model_user->is_admin();


			if (!$data['result'])
			{
				echo "<p> Нет задач. </p>";
				echo "<a href='/task/create' class='btn btn-primary'>Создать первую задачу</a>";
				return;
			}	

			// display sorting parameters
			echo $this->sort_params_div($data['page']);

			foreach ($data['result'] as $key=>$task)
			{
				if ($key !== 'count' && $key !== 'page' && $key !== 'success')
				{
					$complete_word = 'не ';
					if ($task['status'] == '1')
						$complete_word = '';
					$edit_link = '';
					$task['description'] = str_replace("\n", '<br>', $task['description']);

					$edited_by_admin ='';
					if ($task['edited'] == Model_Task::EDITED_WAS_EDITED)
					{
						$edited_by_admin = '(отредактировано администратором)';
					}
					if ($is_admin == 1)
						$edit_link = "
							<a href='/task/edit?id=".$task['id']."' class='btn btn-primary'>Редактировать</a>
						";
					echo "
						<div class='container-fluid'>
							<div class='row task'>
								<div class='col-sm-6'>
									<h2>Задача #".$task['id']." (".$complete_word."выполнена)</h2>
									<div class='name'><b>Автор:</b> ".$task['name']."</br>  
									<b>E-mail:</b><a href='mailto:".$task['email']."'>".$task['email']."</a>
									</div>
									<b>Описание задачи: $edited_by_admin</b>
									<div class='text'>".$task['description']."</div>
									$edit_link
								</div>
							</div>
						</div>
					";
				}
			}
			
			$sort =  false;
			$sort_direction = false;

			if(isset($data['sort']))
				$sort =  $data['sort'];

			if(isset($data['sort_direction']))
				$sort_direction =  $data['sort_direction'];

			echo $this->pagination_div($data['count'], $data['page'], $sort, $sort_direction);
			//include_once 'app/views/template/footer.php';
		}
		
		function create_form()
		{
			$res="
			<form method='post' action='' enctype=\"multipart/form-data\">
				<div class='form-group row'>
					<label for='username' class='col-sm-4'>Ник:</label>
					<input type='text' id='username' name='username' maxlength=100 class='col-sm-4' placeholder='Введите ваш ник'/>
				</div>
				<div class='form-group row'>
					<label for='email' class='col-sm-4'>E-mail:</label>
					<input type='email' name='email' maxlength=100 class='col-sm-4' placeholder='Введите ваш e-mail' id='email'/>
				</div>
				<div class='form-group'>
					<label for='description'>Текст задачи</label>
					<textarea class='form-control' rows=3 placeholder='Введите текст задачи' name='description' id='description'></textarea>
				</div>
				<input type='submit' value='Создать задачу' class='btn btn-primary'>
			</form>
			";
			return ($res);
		}
		
		function generate_create($data = null)
		{
			$title = 'Создание задачи';
			include_once 'application/views/template/header.php';
			echo '<h1>Создание задачи</h1>';
			if ($data == null)
				echo $this->create_form();
			else
			{
				if ($data['success'] == 1)
					echo "<p class='text-success'>Задача создана.</p>
					<a href='/' class='btn btn-success' style='width: 20em; margin: 0.3em;'>Просмотреть список задач</a><br>
					<a href='create' class='btn btn-info' style='width: 20em; margin: 0.3em;'>Создать ещё одну задачу</a><br>
					";
				else
				{
					echo "<p class='text-danger'><b>При создании задачи возникла ошибка:</b>
						".$data['error_text']."<a href='javascript:history.back()' class='btn btn-primary'>Назад</a>
					</p>";
				}
			}
		}
		function generate_edit($data = null)
		{
			$title = "Редактирование задачи";
			$data_id = $data['id'];
			include_once 'application/views/template/header.php';
			echo "<h1>Редактирование задачи #$data_id</h1>";

			if(isset($data['success']))
			{
				if ($data['success'] == 0)
				{
					$error_text = $data['error_text'];
					echo "<p class='text-danger'><b>Ошибка:</b> $error_text</p>";
					echo "<a href='javascript:history.back()' class='btn btn-primary'>Назад</a>";
				} 
				else 
				{
					echo "<p class='text-success'>Задача отредактирована.</p>";
					echo "<a href='/task/index' class='btn btn-info' style='width: 20em; margin: 0.3em;'>К списку задач</a>";
				}
			}

			if (isset($data['result'][0]))
				echo $this->edit_form($data);
		}

		function edit_form($data)
		{
			$checked = '';

			if ($data['result']['status'] == Model_Task::STATUS_COMPLETE)
				$checked = 'checked';
			$description = $data['result']['description'];
			$res="
			<form method='post'>
				<div class='form-group'>
					<label for='description'>Текст задачи</label>
					<textarea class='form-control' rows=3 placeholder='Теги и специальные символы будут убраны!' name='description' id='text'>$description</textarea>
				</div>
				<div class='checkbox'>
					<label>
					  <input type='checkbox' name='status' $checked/> Задача выполнена
					</label>
				  </div>
				<input type='submit' value='Применить изменения' class='btn btn-primary'>
			</form>
			";
			return ($res);
		}
	}